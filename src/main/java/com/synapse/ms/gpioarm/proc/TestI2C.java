package com.synapse.ms.gpioarm.proc;

import com.pi4j.gpio.extension.mcp.MCP23017GpioProvider;
import com.pi4j.gpio.extension.mcp.MCP23017Pin;
import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;

import java.io.IOException;

public class TestI2C {

    private GpioController gpio;
    private MCP23017GpioProvider provider;
    private GpioPinDigitalInput[] myInputs;
    private GpioPinDigitalOutput[] myOutputs;
    private boolean toggle;

    public void init() throws IOException, I2CFactory.UnsupportedBusNumberException {
        provider = new MCP23017GpioProvider(I2CBus.BUS_0, 0x20);

        gpio = GpioFactory.getInstance();

        myInputs = new GpioPinDigitalInput[] {
                gpio.provisionDigitalInputPin(provider, MCP23017Pin.GPIO_B0, "MyInput-A0", PinPullResistance.PULL_UP),
                gpio.provisionDigitalInputPin(provider, MCP23017Pin.GPIO_B1, "MyInput-A1", PinPullResistance.PULL_UP),
                gpio.provisionDigitalInputPin(provider, MCP23017Pin.GPIO_B2, "MyInput-A2", PinPullResistance.PULL_UP),
                gpio.provisionDigitalInputPin(provider, MCP23017Pin.GPIO_B3, "MyInput-A3", PinPullResistance.PULL_UP),
                gpio.provisionDigitalInputPin(provider, MCP23017Pin.GPIO_B4, "MyInput-A4", PinPullResistance.PULL_UP),
                gpio.provisionDigitalInputPin(provider, MCP23017Pin.GPIO_B5, "MyInput-A5", PinPullResistance.PULL_UP),
                gpio.provisionDigitalInputPin(provider, MCP23017Pin.GPIO_B6, "MyInput-A6", PinPullResistance.PULL_UP),
                gpio.provisionDigitalInputPin(provider, MCP23017Pin.GPIO_B7, "MyInput-A7", PinPullResistance.PULL_UP),
        };

        myOutputs = new GpioPinDigitalOutput[] {
                gpio.provisionDigitalOutputPin(provider, MCP23017Pin.GPIO_A0, "MyOutput-B0", PinState.LOW),
                gpio.provisionDigitalOutputPin(provider, MCP23017Pin.GPIO_A1, "MyOutput-B1", PinState.LOW),
                gpio.provisionDigitalOutputPin(provider, MCP23017Pin.GPIO_A2, "MyOutput-B2", PinState.LOW),
                gpio.provisionDigitalOutputPin(provider, MCP23017Pin.GPIO_A3, "MyOutput-B3", PinState.LOW),
                gpio.provisionDigitalOutputPin(provider, MCP23017Pin.GPIO_A4, "MyOutput-B4", PinState.LOW),
                gpio.provisionDigitalOutputPin(provider, MCP23017Pin.GPIO_A5, "MyOutput-B5", PinState.LOW),
                gpio.provisionDigitalOutputPin(provider, MCP23017Pin.GPIO_A6, "MyOutput-B6", PinState.LOW),
                gpio.provisionDigitalOutputPin(provider, MCP23017Pin.GPIO_A7, "MyOutput-B7", PinState.LOW)
        };

        gpio.addListener(new GpioPinListenerDigital() {
            @Override
            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
                // display pin state on console
                System.out.println(" --> GPIO PIN STATE CHANGE: " + event.getPin() + " = "
                        + event.getState());

                if (PinState.LOW.equals(event.getState())) {
                    toggle = !toggle;
                    gpio.setState(toggle, myOutputs);
                }
            }
        }, myInputs);

        try {
            for (int count = 0; count < 10; count++) {
                gpio.setState(true, myOutputs);
                Thread.sleep(1000);

                gpio.setState(false, myOutputs);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // stop all GPIO activity/threads by shutting down the GPIO controller
        // (this method will forcefully shutdown all GPIO monitoring threads and scheduled tasks)
        //gpio.shutdown();

        /*I2CBus i2c = I2CFactory.getInstance(I2CBus.BUS_0);
        System.out.println("I2C "+i2c);

        I2CDevice device = i2c.getDevice((byte)0x20);
        System.out.println("device "+device);*/

        //int response = device.read(MCP23017Pin.GPIO_A6.getAddress());
        //System.out.println("ID = " + String.format("0x%02x", response));

        /*device.write(MCP23017Pin.GPIO_A0.getAddress(), (byte)0x01);
        device.write(MCP23017Pin.GPIO_A1.getAddress(), (byte)0x01);
        device.write(MCP23017Pin.GPIO_A2.getAddress(), (byte)0x01);
        device.write(MCP23017Pin.GPIO_A3.getAddress(), (byte)0x01);
        device.write(MCP23017Pin.GPIO_A4.getAddress(), (byte)0x01);
        device.write(MCP23017Pin.GPIO_A5.getAddress(), (byte)0x01);*/
        //device.write(MCP23017Pin.GPIO_A6.getAddress(), (byte)0x40);
        /*device.write(MCP23017Pin.GPIO_A7.getAddress(), (byte)0x01);*/
    }

    public void execute() {
        //gpio.setState(toggle, myOutputs);
        //toggle = !toggle;
    }
}
