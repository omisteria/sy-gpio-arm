package com.synapse.ms.gpioarm.proc;

import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import com.pi4j.platform.Platform;
import com.pi4j.platform.PlatformAlreadyAssignedException;
import com.pi4j.platform.PlatformManager;
import com.pi4j.util.ConsoleColor;

public class TestGPIO {

    private GpioPinDigitalOutput output1;
    private GpioPinDigitalOutput output2;

    private GpioPinDigitalInput input;

    public TestGPIO() {

    }

    public void init() throws PlatformAlreadyAssignedException {
        final GpioController gpio = GpioFactory.getInstance();

        // #    Physical    wPi                     BCM

        // +1    3           OrangePiPin.GPIO_08     GPIO 12
        // +2    5           OrangePiPin.GPIO_09     GPIO 11
        // +3    7           OrangePiPin.GPIO_07     GPIO 6
        // 4    8           OrangePiPin.GPIO_15     GPIO 13 (0)
        // 5    10          OrangePiPin.GPIO_16     GPIO 14 (1)
        // +6    11          OrangePiPin.GPIO_0      GPIO 1
        // 7    12          OrangePiPin.GPIO_1      GPIO 110 (107)
        // 8    13          OrangePiPin.GPIO_2      GPIO 0 (353)
        // +9    15          OrangePiPin.GPIO_3      GPIO 3
        // 10   16          OrangePiPin.GPIO_4      GPIO 68 (19)
        // 11   18          OrangePiPin.GPIO_5      GPIO 71 (18)
        // 12   19          OrangePiPin.GPIO_12     GPIO 64
        // 13   21          OrangePiPin.GPIO_13     GPIO 65
        // +14   22          OrangePiPin.GPIO_6      GPIO 2
        // 15   23          OrangePiPin.GPIO_14     GPIO 66
        // 16   24          OrangePiPin.GPIO_10     GPIO 67
        // 17   26          OrangePiPin.GPIO_11     GPIO 21


        // provision gpio pin as an output pin and turn on
        //output1 = gpio.provisionDigitalOutputPin(OrangePiPin.GPIO_07, "LCD green", PinState.HIGH); // GPIO 6
        output2 = gpio.provisionDigitalOutputPin(OrangePiPin.GPIO_08, "LCD red", PinState.HIGH);
        output2.setShutdownOptions(false, PinState.LOW);
        output2.addListener(new GpioPinListenerDigital() {
            @Override
            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
                // display pin state on console
                System.out.println(" --> GPIO PIN STATE CHANGE: " + event.getPin() + " = " +
                        ConsoleColor.conditional(
                                event.getState().isHigh(), // conditional expression
                                ConsoleColor.GREEN,        // positive conditional color
                                ConsoleColor.RED,          // negative conditional color
                                event.getState()));        // text to display
            }
        });


        // provision gpio pin as an input pin
        //input = gpio.provisionDigitalInputPin(pin, "MyInput", PinPullResistance.PULL_UP);

        // set shutdown state for this pin: unexport the pin
        //input.setShutdownOptions(true);
    }

    public void execute() {
        //output1.toggle();
        output2.toggle();
    }
}
