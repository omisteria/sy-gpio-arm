package com.synapse.ms.gpioarm.services;

import com.pi4j.platform.PlatformManager;
import com.pi4j.system.NetworkInfo;
import com.pi4j.system.SystemInfo;
import com.synapse.ms.gpioarm.domain.SysInfoItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SystemService {

    public Map getSystemInfo() {

        Map<String, List<SysInfoItem>> res = new HashMap();

        List<SysInfoItem> g1 = new ArrayList<>();
        res.put("platform", g1);

        try {g1.add(new SysInfoItem("platformName", "Platform Name", PlatformManager.getPlatform().getLabel()));}
        catch(UnsupportedOperationException ex){}
        try {g1.add(new SysInfoItem("platformId", "Platform ID", PlatformManager.getPlatform().getId()));}
        catch(UnsupportedOperationException ex){}


        List<SysInfoItem> g2 = new ArrayList<>();
        res.put("hardware", g2);

        try {g2.add(new SysInfoItem("serial", "Serial Number", SystemInfo.getSerial()));}
        catch(Exception ex){}
        try {g2.add(new SysInfoItem("cpuRevision", "CPU Revision", SystemInfo.getCpuRevision()));}
        catch(Exception ex){}
        try {g2.add(new SysInfoItem("cpuArchitecture", "CPU Architecture", SystemInfo.getCpuArchitecture()));}
        catch(Exception ex){}
        try {g2.add(new SysInfoItem("cpuPart", "CPU Part", SystemInfo.getCpuPart()));}
        catch(Exception ex){}
        try {g2.add(new SysInfoItem("cpuTemperature", "CPU Temperature", Float.toString(SystemInfo.getCpuTemperature() / 1000)));}
        catch(Exception ex){}
        try {g2.add(new SysInfoItem("cpuCoreVoltage", "CPU Core Voltage", Float.toString(SystemInfo.getCpuVoltage())));}
        catch(Exception ex){}
        try {g2.add(new SysInfoItem("cpuModelName", "CPU Model Name", SystemInfo.getModelName()));}
        catch(Exception ex){}
        try {g2.add(new SysInfoItem("processor", "Processor", SystemInfo.getProcessor()));}
        catch(Exception ex){}
        try {g2.add(new SysInfoItem("hw", "Hardware", SystemInfo.getHardware()));}
        catch(Exception ex){}
        try {g2.add(new SysInfoItem("hwRevision", "Hardware Revision", SystemInfo.getRevision()));}
        catch(Exception ex){}
        try {g2.add(new SysInfoItem("hwFloatAbi", "Is Hard Float ABI", Boolean.toString(SystemInfo.isHardFloatAbi())));}
        catch(Exception ex){}
        try {g2.add(new SysInfoItem("boardType", "Board Type", SystemInfo.getBoardType().name()));}
        catch(Exception ex){}


        List<SysInfoItem> g3 = new ArrayList<>();
        res.put("memory", g3);

        try {g3.add(new SysInfoItem("total", "Total Memory", Long.toString(SystemInfo.getMemoryTotal())));}
        catch(Exception ex){}
        try {g3.add(new SysInfoItem("used", "Used Memory", Long.toString(SystemInfo.getMemoryUsed())));}
        catch(Exception ex){}
        try {g3.add(new SysInfoItem("free", "Free Memory", Long.toString(SystemInfo.getMemoryFree())));}
        catch(Exception ex){}
        try {g3.add(new SysInfoItem("shared", "Shared Memory", Long.toString(SystemInfo.getMemoryShared())));}
        catch(Exception ex){}
        try {g3.add(new SysInfoItem("buffers", "Memory Buffers", Long.toString(SystemInfo.getMemoryBuffers())));}
        catch(Exception ex){}
        try {g3.add(new SysInfoItem("cached", "Cached Memory", Long.toString(SystemInfo.getMemoryCached())));}
        catch(Exception ex){}



        List<SysInfoItem> g4 = new ArrayList<>();
        res.put("os", g4);

        try {g4.add(new SysInfoItem("name", "OS Name", SystemInfo.getOsName()));}
        catch(Exception ex){}
        try {g4.add(new SysInfoItem("version", "OS Version", SystemInfo.getOsVersion()));}
        catch(Exception ex){}
        try {g4.add(new SysInfoItem("arch", "OS Architecture", SystemInfo.getOsArch()));}
        catch(Exception ex){}
        try {g4.add(new SysInfoItem("firmwareBuild", "OS Firmware Build", SystemInfo.getOsFirmwareBuild()));}
        catch(Exception ex){}
        try {g4.add(new SysInfoItem("firmwareDate", "OS Firmware Date", SystemInfo.getOsFirmwareDate()));}
        catch(Exception ex){}


        List<SysInfoItem> g5 = new ArrayList<>();
        res.put("java", g5);

        g5.add(new SysInfoItem("vendor", "Java Vendor", SystemInfo.getJavaVendor()));
        g5.add(new SysInfoItem("vendorUrl", "Java Vendor URL", SystemInfo.getJavaVendorUrl()));
        g5.add(new SysInfoItem("version", "Java Version", SystemInfo.getJavaVersion()));
        g5.add(new SysInfoItem("vm", "Java VM", SystemInfo.getJavaVirtualMachine()));
        g5.add(new SysInfoItem("runtime", "Java Runtime", SystemInfo.getJavaRuntime()));


        List<SysInfoItem> g6 = new ArrayList<>();
        res.put("network", g6);

        try {g6.add(new SysInfoItem("hostname", "Hostname", NetworkInfo.getHostname()));}
        catch(Exception ex){}
        try {
            for (String ipAddress : NetworkInfo.getIPAddresses())
            g6.add(new SysInfoItem("ip", "IP Address", ipAddress));
        }
        catch(Exception ex){}
        try {
            for (String fqdn : NetworkInfo.getFQDNs())
            g6.add(new SysInfoItem("fqdn", "FQDN", fqdn));
        }
        catch(Exception ex){}
        try {
            for (String nameserver : NetworkInfo.getNameservers())
            g6.add(new SysInfoItem("nameserver", "Nameserver", nameserver));
        }
        catch(Exception ex){}


        return res;
    }
}
