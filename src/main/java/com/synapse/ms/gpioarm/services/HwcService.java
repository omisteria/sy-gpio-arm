package com.synapse.ms.gpioarm.services;

import com.synapse.ms.gpioarm.dao.UnitsDAO;
import com.synapse.ms.gpioarm.domain.HwcUnit;
import com.synapse.ms.gpioarm.domain.HwcUnitSet;

public class HwcService {

    private UnitsDAO unitsDAO;

    public HwcService(UnitsDAO unitsDAO) {
        this.unitsDAO = unitsDAO;
    }

    public HwcUnitSet getUnitSetById(String id) {
        return unitsDAO.getUnitById(id);
    }
}
