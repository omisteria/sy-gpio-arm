package com.synapse.ms.gpioarm.web;

import com.google.gson.Gson;
import com.synapse.ms.gpioarm.dao.DAO;
import com.synapse.ms.gpioarm.domain.HwcUnitSet;
import com.synapse.ms.gpioarm.domain.StatusResponse;
import com.synapse.ms.gpioarm.dto.StandardResponse;
import com.synapse.ms.gpioarm.services.HwcService;
import spark.Route;

public class UnitApi {

    DAO dao = new DAO();
    private HwcService hwcService = new HwcService(dao.getUnitsDAO());

    public UnitApi() {
    }

    public Route getUnitSet = (request, response) -> {
        response.type("application/json");

        String unitSetId = request.params(":id");
        HwcUnitSet unitSet = hwcService.getUnitSetById(unitSetId);

        if (unitSet == null) {
            return new Gson().toJson(
                    new StandardResponse(
                            StatusResponse.ERROR,
                            new Gson().toJson("UnitSet not found")
                    )
            );
        }

        return new Gson().toJson(
                new StandardResponse(
                        StatusResponse.SUCCESS,
                        new Gson().toJsonTree(unitSet))
        );
    };

    public Route saveUnitSet = (request, response) -> {
        //...
        return null;
    };

    public Route deleteUnitSet = (request, response) -> {
        //...
        return null;
    };
}
