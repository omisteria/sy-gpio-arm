package com.synapse.ms.gpioarm.web;

import com.google.gson.Gson;
import com.synapse.ms.gpioarm.domain.HwcConfig;
import com.synapse.ms.gpioarm.domain.StatusResponse;
import com.synapse.ms.gpioarm.dto.StandardResponse;
import com.synapse.ms.gpioarm.services.SystemService;
import spark.Request;
import spark.Response;
import spark.Route;

public class ConfigApi {

    public SystemService systemService = new SystemService();

    public Route getConfig = (request, response) -> {
        response.type("application/json");

        //HwcConfig config = new Gson().fromJson(request.body(), User.class);
        HwcConfig config = new HwcConfig();
        //userService.addUser(user);

        return new Gson().toJson(
                new StandardResponse(
                        StatusResponse.SUCCESS,
                        new Gson().toJsonTree(config)
                )
        );
    };

    public Route getSystemInfo = (request, response) -> {
        response.type("application/json");

        return new Gson().toJson(
                new StandardResponse(
                        StatusResponse.SUCCESS,
                        new Gson().toJsonTree(systemService.getSystemInfo())
                )
        );
    };
}
