package com.synapse.ms.gpioarm.dao;

import com.synapse.ms.gpioarm.domain.HwcUnit;
import com.synapse.ms.gpioarm.domain.HwcUnitSet;
import org.sql2o.Connection;


public class UnitsDAO {


    public UnitsDAO() {
    }

    public HwcUnitSet getUnitById(String id) {
        String sql =
                "SELECT id, description, duedate " +
                        "FROM tasks";

        try(Connection con = DAO.sql2o.open()) {
            return con.createQuery(sql)
                    .executeAndFetchFirst(HwcUnitSet.class);
        }
    }
}
