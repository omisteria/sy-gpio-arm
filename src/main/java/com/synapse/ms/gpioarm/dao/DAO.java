package com.synapse.ms.gpioarm.dao;

import org.h2.jdbcx.JdbcDataSource;
import org.sql2o.Sql2o;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class DAO {

    private static JdbcDataSource dataSource;

    private static final String DB_DRIVER = "org.h2.Driver";
    private static final String DB_CONNECTION = "jdbc:h2:~/gpio-arm";
    private static final String DB_USER = "sa";
    private static final String DB_PASSWORD = "sa";

    public static Sql2o sql2o;

    static {

        try {
            dataSource = new JdbcDataSource();
            dataSource.setURL(DB_CONNECTION);
            dataSource.setUser(DB_USER);
            dataSource.setPassword(DB_PASSWORD);

            sql2o = new Sql2o(dataSource);

        } catch (Exception e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    private UnitsDAO unitsDAO;

    public DAO() {
        unitsDAO = new UnitsDAO();
    }

    public Sql2o getSql2o() {
        return sql2o;
    }

    public UnitsDAO getUnitsDAO() {
        return unitsDAO;
    }

    public void addUnit(Map<String, Object> data) {
        /*QueryRunner run = new QueryRunner( dataSource );

        try	{
            int inserts = run.update( "INSERT INTO Person (first_name, last_name) VALUES (?,?)",
                    data.get("first_name"),
                    data.get("last_name"));

        } catch(SQLException sqle) {
            throw new RuntimeException("Problem updating", sqle);
        }*/
    }

    public List<String> loadPeople() {

        /*QueryRunner run = new QueryRunner(dataSource);

        try	{

            ResultSetHandler<List<String>> h = new BeanListHandler<String>(String.class,
                    new BasicRowProcessor(new GenerousBeanProcessor())); // 1

            List<String> persons = run.query("SELECT * FROM Person", h);

            return persons;

        } catch(SQLException sqle) {
            throw new RuntimeException("Problem updating", sqle);
        }*/
        return null;

    }

    private Connection getDBConnection() {
        Connection dbConnection = null;
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        try {
            dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
            return dbConnection;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return dbConnection;
    }


}
