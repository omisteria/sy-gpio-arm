package com.synapse.ms.gpioarm;

import com.pi4j.io.i2c.I2CFactory;
import com.pi4j.platform.Platform;
import com.pi4j.platform.PlatformAlreadyAssignedException;
import com.pi4j.platform.PlatformManager;
import com.synapse.ms.gpioarm.dao.DAO;
import com.synapse.ms.gpioarm.proc.TestGPIO;
import com.synapse.ms.gpioarm.proc.TestI2C;
import com.synapse.ms.gpioarm.web.ApiController;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static spark.Spark.*;

public class GpioArmApplication {

    ScheduledExecutorService scheduler =
            Executors.newScheduledThreadPool(5);

    //TestGPIO testGPIO = new TestGPIO();
    TestI2C testI2C = new TestI2C();

    ApiController apiController = new ApiController();


	private void init() {

	    DAO dao = new DAO();
	    dao.getUnitsDAO();

        try {
            PlatformManager.setPlatform(Platform.ORANGEPI);
        } catch (PlatformAlreadyAssignedException e) {
            e.printStackTrace();
        }

        port(8080);
        options("/*", (request,response)->{

            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if(accessControlRequestMethod != null){
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            return "OK";
        });

        before((request,response)->{
            response.header("Access-Control-Allow-Origin", "*");
        });

        path("/api", () -> {
            // before("/*", (q, a) -> log.info("Received api call"));
            path("/config", () -> {
                get("/sysinfo", apiController.getConfigApi().getSystemInfo);
                get("/get", apiController.getConfigApi().getConfig);
            });

            path("/unitset", () -> {
                get("/:id", apiController.getUnitApi().getUnitSet);
                post("/:id", apiController.getUnitApi().saveUnitSet);
                delete("/:id", apiController.getUnitApi().deleteUnitSet);
            });
        });

        get("example", (req, res) -> {
            Map<String, Object> model = new HashMap<>();
            model.put("name", "TestName");
            model.put("rowList", Arrays.asList(new String[] {"one", "two"}));

            return new VelocityTemplateEngine().render(
                    new ModelAndView(model, "main.template.vm")
            );
        });

        /*try {
            testGPIO.init();
        } catch (PlatformAlreadyAssignedException e) {
            e.printStackTrace();
        }*/
        try {
            testI2C.init();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (I2CFactory.UnsupportedBusNumberException e) {
            e.printStackTrace();
        }

        //scheduler.scheduleAtFixedRate(testGPIO::execute,1000, 2000, TimeUnit.MILLISECONDS);
        scheduler.scheduleAtFixedRate(testI2C::execute,1000, 2000, TimeUnit.MILLISECONDS);
    }

    public static void main(String[] args) {
        new GpioArmApplication().init();
    }

    /*public Sql2o dbh2() {
        String dataSource = "jdbc:h2:~/app123.db";

        String conString = dataSource + ";INIT=RUNSCRIPT from 'classpath:db/init.sql'";
        Sql2o sql2o = new Sql2o(conString, "", "");
        FoodDao foodDao = new Sql2oFoodDao(sql2o);
        Connection con = sql2o.open();
    }*/
}
