package com.synapse.ms.gpioarm.domain;

public class HwcConfig {

    private String identifier;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
}
