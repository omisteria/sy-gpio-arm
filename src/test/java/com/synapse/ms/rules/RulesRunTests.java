package com.synapse.ms.rules;

import com.sun.javafx.application.PlatformImpl;
import jdk.nashorn.api.scripting.JSObject;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.script.*;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.function.Consumer;

public class RulesRunTests {

    private ScriptEngineManager scriptEngineManager;
    private ScriptEngine scriptEngine;

    @Before
    public void prepare() {
        scriptEngineManager = new ScriptEngineManager();
        scriptEngine = scriptEngineManager.getEngineByName("nashorn");

        try {
            InputStream is = getClass().getClassLoader().getResourceAsStream("env.js");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            scriptEngine.eval(bufferedReader);
        } catch (Exception e) {
            e.printStackTrace();
        }

        PlatformImpl.startup(() -> {});
    }

    @Test
    public void testInvokeDelegate() {
        try {
            scriptEngine.eval("function foo(callback) { callback('bar') }");
            ((Invocable)scriptEngine).invokeFunction("foo", (Consumer<String>)IntMethods::print);

            Assert.assertTrue(true);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (ScriptException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetResultOfFunction() {
        final Compilable compilable = (Compilable) scriptEngine;
        final Invocable invocable = (Invocable) scriptEngine;

        try {
            final String statement =
                    "function fetch(value, count) { count++ ; return {value: value, count : count} };";
            final CompiledScript compiled = compilable.compile(statement);

            compiled.eval();

            JSObject jso = (JSObject) invocable.invokeFunction("fetch", 10.0, 2);
            System.out.println("value=" + jso.getMember("value"));
            System.out.println("count=" + jso.getMember("count"));

            Assert.assertEquals(jso.getMember("value"), 10.0);
            Assert.assertEquals(jso.getMember("count"), 3.0);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testInvokeJavaObjectStaticMethod() {

        String script = "var MyJavaClass = Java.type('com.synapse.ms.rules.RulesRunTests.JHandler'); " +
                " var greetingResult = MyJavaClass.sayHello('John Doe');" +
                " print(greetingResult);";
        try {
            scriptEngine.eval(script);

            Assert.assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testInvokeJavaObjectMethod() {

        String script = "var MyJavaClass = Java.type('com.synapse.ms.rules.RulesRunTests.JHandler'); " +
                " var myClass = new MyJavaClass();" +
                " var calcResult = myClass.add(1, 2);" +
                " print(calcResult);";
        try {
            scriptEngine.eval(script);

            Assert.assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testInvokeJavaScriptObjectMethod() {

        String script = "function Person(firstName, lastName) {\n" +
                "  this.firstName = firstName;\n" +
                "  this.lastName = lastName;\n" +
                "  this.getFullName = function() {\n" +
                "    return this.firstName + ' ' + this.lastName;\n" +
                "  }\n" +
                "}\n" +
                " var MyJavaClass = Java.type('com.synapse.ms.rules.RulesRunTests.IntMethods');\n" +
                " var person = new Person('John', 'Doe');\n" +
                " MyJavaClass.getFullName(person);";
        try {
            scriptEngine.eval(script);

            Assert.assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testJSTimeout() {
        String script = "setTimeout(function() {print('from setTimeout');}, 1000);\n" +
                "print('after setTimeout');";
        try {
            scriptEngine.eval(script);

            Assert.assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testLoadRuleFromFile() {
        String script = "load('src/test/java/com/synapse/ms/rules/rule1.js')";
        try {
            scriptEngine.eval(script);

            Assert.assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static class JHandler {

        public static String sayHello(String name) {
            return String.format("Hello %s from Java!", name);
        }

        public int add(int a, int b) {
            return a + b;
        }
    }

    public static class IntMethods {

        public static void print(String arg) {
            System.out.println("="+arg);
        }

        public static void getFullName(ScriptObjectMirror person) {
            System.out.println("Full name is: " + person.callMember("getFullName"));
        }
    }
}
