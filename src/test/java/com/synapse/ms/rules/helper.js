'use strict';

(function (context) {

    var logger 	= Java.type("org.slf4j.LoggerFactory").getLogger("com.synapse.automation.script.rulesupport.internal.shared.SimpleRule");
    var uuid 	= Java.type("java.util.UUID");

    context.logInfo = function(type , value) {
        logger.info(args(arguments));
    };

})(this);