package com.synapse.ms.gpioarm;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.synapse.ms.gpioarm.dao.UnitsDAO;
import com.synapse.ms.gpioarm.domain.HwcUnit;
import com.synapse.ms.gpioarm.domain.HwcUnitSet;
import com.synapse.ms.gpioarm.services.HwcService;
import org.easymock.EasyMock;
import org.junit.Test;

public class UnitsCRUDTests {

    @Test
    public void readAllUnits() {

        String unitId = "test-id";
        HwcUnitSet expRes = new HwcUnitSet();
        expRes.setId(unitId);

        UnitsDAO unitsDAO = EasyMock.createMock(UnitsDAO.class);
        expect(unitsDAO.getUnitById(unitId)).andReturn(expRes);
        replay(unitsDAO);

        HwcService hwcService = new HwcService(unitsDAO);
        HwcUnitSet res = hwcService.getUnitSetById(unitId);
        assertEquals(res.getId(), unitId);
    }
}
